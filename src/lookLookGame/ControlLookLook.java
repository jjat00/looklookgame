/*
 * Archivo: ControlLookLook.java
 * Fecha: 24 de Noviembre de 2019
 * Autores: Jaimen Aza            1923556
 *          Valentina Salamanca   1842427
 */
package lookLookGame;

// TODO: Auto-generated Javadoc
/**
 * The Class ControlLookLook. 
 * Clase que maneja la lógica del juego y procesos en la clase GUILookLook,
 * es la mediadora para ocultar, mostrar, comparar las cartas y terminar el juego.
 * Es necesaria para la clase GUILookLook
 */
public class ControlLookLook {

	/** The estadoCasilla. Guarda el estado actual de cada casilla en el tablero*/
	private boolean estadoCasilla[][];

	/** The anchoTablero. Determina el ancho del tablero (nivel básico->4 y avanzado->5) */
	private int anchoTablero;

	/** The alto tablero. Determina el alto del tablero (nivel básico-> 3 y avanzado para 4 */
	private int altoTablero;

	/**
	 * Instantiates a new control look look.
	 * inicia el valor por defecto  del ancho y alto del tablero.
	 * Crea la matriz de estados de las casillas
	 *
	 * @param columna the columna
	 * @param fila the fila
	 */
	public ControlLookLook(int columna,int fila){
		anchoTablero = columna;
		altoTablero = fila;
		estadoCasilla = new boolean[anchoTablero][altoTablero];
	}

	/**
	 * Estado casilla.
	 *
	 * @param columna the columna
	 * @param fila the fila
	 * @return true, if las dos cartas son iguales
	 */
	public boolean estadoCasilla(int columna, int fila) {
		return estadoCasilla[columna][fila];
	}

	/**
	 * Negar estado casilla.
	 * Si las dos cartas no son iguales  el estado de las casillas vuelve a ser false
	 * 
	 * @param columna the columna
	 * @param fila the fila
	 */
	public void negarEstadoCasilla(int columna, int fila) {
		estadoCasilla[columna][fila] = !estadoCasilla[columna][fila];
	}

	/**
	 * Terminar juego.
	 *
	 * @return true, if el numero de casillas en estado true es igual al total de casillas del tablero
	 */
	public boolean terminarJuego() {
		int contadorCarasVisibles = 0;
		for (int columna = 0; columna < anchoTablero; columna++) {
			for (int fila = 0; fila < altoTablero; fila++) {
				if (estadoCasilla[columna][fila])
					contadorCarasVisibles++;
			}
		}
		if (contadorCarasVisibles == anchoTablero * altoTablero)
			return true;
		return false;
	}
}