/*
 * Archivo: GUILookLook.java
 * Fecha: 24 de Noviembre de 2019
 * Autores: Jaimen Aza               1923556
 *          Valentina Salamanca      1842427
 */
package lookLookGame;

import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

// TODO: Auto-generated Javadoc
/**
 * The Class GUILooklook.
 * Posee los elementos de la interfaz de usuario, el tablero, la ventana de configuración 
 * y todos los elementos gráficos.
 * Tiene relación con la clase ControlLookLook y con la clase Tablero
 */
public class GUILooklook extends JFrame {

	/** The container para la zona del juego. */
	private Container containerZonaJuego;

	/** The container para la zona de la configuracion del juego. */
	private Container containerConfiguracionJuego;

	/** The panel para los elementos de la zona configuracion juego. */
	private JPanel panelConfiguracionJuego;

	/** The panel combo box para esocoger tema y nivel del juego. */
	private JPanel panelComboBox;

	/** The panel de la zona juego donde se ubica cada parejas de cartas. */
	private JPanel panelZonaJuego;

	/** The carta. Matriz para almecenar cada pareja de cartas. */
	private JLabel carta[][];

	/** The logo del juego. */
	private JLabel logo;

	/** The texto configuracion del juego. */
	private JLabel textoConfiguracionJuego;

	/** The boton jugar para empezar el juego. */
	private JButton botonJugar;

	/** The combo box para escoger el tema. */
	private JComboBox comboBoxTema;

	/** The combo box para escoger nivel. */
	private JComboBox comboBoxNivel;

	/** The borde para asiganar a cada carta. */
	private Border borde;

	/** The tablero. Crea una matriz aleatoria de pareja de cartas*/
	private Tablero tablero;

	/** The control. */
	private ControlLookLook control;

	/** The escucha de cada carta. */
	private EscuchaZonaJuego escuchaCarta;

	/** The escucha de los combo box. */
	private EscuchaComboBox escuchaComboBox;

	/** The escucha del boton jugar. */
	private EscuchaConfiguracionJuego escuchaBotonJugar;

	/** The tema seleccionado. */
	private String temaSeleccionado;

	/** The nivel seleccionado. */
	private String nivelSeleccionado;

	/** The ancho del tablero. */
	private int anchoTablero;

	/** The alto del tablero. */
	private int altoTablero;



	/**
	 * Instantiates a new GUI looklook.
	 * Asigna los valores iniciales a la interfaz del juego. 
	 * Por defecto el juego empieza en nivel básico y con el tema de Naruto.
	 */
	public GUILooklook(){
		super("Look Look Game");
		anchoTablero = 4;
		altoTablero = 3;
		temaSeleccionado = "Naruto";
		nivelSeleccionado = "Básico";
		initVentanaConfiguracionDeJuego();
		// Set default window configuration
		this.setUndecorated(false);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Inits the ventana configuracion de juego.
	 * Inicializa cada componente en la primer venta donde 
	 * el usuario selecciona el tema y difucultad del juego.
	 */
	public void initVentanaConfiguracionDeJuego() {
		//JFrame container zona configuracion del juego and layout
		containerConfiguracionJuego = getContentPane();

		panelConfiguracionJuego = new JPanel();
		panelConfiguracionJuego.setBackground(new Color(0, 0, 0));
		panelConfiguracionJuego.setLayout(new BorderLayout());

		panelComboBox = new JPanel();
		panelComboBox.setBackground(new Color(0, 0, 0));
		panelComboBox.setLayout(new FlowLayout());

		//create Listener object y Logic control
		escuchaBotonJugar = new EscuchaConfiguracionJuego();
		escuchaComboBox = new EscuchaComboBox();

		//set components configuration
		botonJugar = new JButton("Jugar!");
		botonJugar.setBackground(new Color(0, 0, 0));
		botonJugar.setForeground(new Color(255,255,255));
		botonJugar.addMouseListener(escuchaBotonJugar);
		botonJugar.setPreferredSize(new Dimension(90, 40));

		textoConfiguracionJuego = new JLabel("Selecciona un tema y un nivel");
		textoConfiguracionJuego.setBackground(new Color(0,0,0));
		textoConfiguracionJuego.setForeground(new Color(100,0,0));
		textoConfiguracionJuego.setHorizontalAlignment(0);
		textoConfiguracionJuego.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));

		logo = new JLabel();
		logo.setIcon(new ImageIcon("src/imagenes/logo.png"));

		String[] temas = { "Naruto", "Hora de aventura" };
		comboBoxTema = new JComboBox<>(temas);
		comboBoxTema.addActionListener(escuchaComboBox);
		comboBoxTema.setPreferredSize(new Dimension(150, 40));
		comboBoxTema.setBackground(new Color(0, 0, 0));
		comboBoxTema.setForeground(new Color(255, 255, 255));

		String[] nivel = { "Básico", "Avanzado" };
		comboBoxNivel = new JComboBox<>(nivel);
		comboBoxNivel.addActionListener(escuchaComboBox);
		comboBoxNivel.setPreferredSize(new Dimension(150, 40));
		comboBoxNivel.setBackground(new Color(0, 0, 0));
		comboBoxNivel.setForeground(new Color(255, 255, 255));

		panelComboBox.add(comboBoxTema);
		panelComboBox.add(comboBoxNivel);
		panelComboBox.add(botonJugar);

		panelConfiguracionJuego.add(logo, BorderLayout.NORTH);
		panelConfiguracionJuego.add(textoConfiguracionJuego,BorderLayout.CENTER);
		panelConfiguracionJuego.add(panelComboBox, BorderLayout.SOUTH);
		containerConfiguracionJuego.add(panelConfiguracionJuego);
	}

	/**
	 * The Class EscuchaComboBox. 
	 * Escucha el nivel y el tema seleccionado por el usario 
	 */
	private class EscuchaComboBox implements ActionListener {
		/**
		 * Action performed.
		 *
		 * @param event the event
		 */
		public void actionPerformed(ActionEvent event) {
			temaSeleccionado = comboBoxTema.getSelectedItem().toString();
			nivelSeleccionado = comboBoxNivel.getSelectedItem().toString();
		}
	}

	/**
	 * The Class EscuchaConfiguracionJuego.
	 * Empieza el juego con las configuraciones seleccionadas por el usuario 
	 */
	private class EscuchaConfiguracionJuego  extends MouseAdapter{

		/**
		 * Mouse clicked.
		 *
		 * @param event the event
		 */
		public void mouseClicked(MouseEvent event) {
			if (event.getSource() == botonJugar) {
				if (nivelSeleccionado == "Básico") {
					anchoTablero = 4;
					altoTablero = 3;
					initTablero();
					initVentanaZonaDeJuego();
				}else{
					anchoTablero = 5;
					altoTablero = 4;
					initTablero();
					initVentanaZonaDeJuego();
				}
				panelConfiguracionJuego.setVisible(false);
				panelZonaJuego.setVisible(true);
				pack();
				setLocationRelativeTo(null);
			}
		}

		/**
		 * Inits the tablero.
		 * Inicializa el coontrol del juego y el tablero. Genera un vector de parejas aleatorias de imagenes
		 * y llena la matriz del tablero
		 */
		private void initTablero() {
			control = new ControlLookLook(anchoTablero, altoTablero);
			tablero = new Tablero(anchoTablero, altoTablero);
			tablero.createRandomNameImage();
			tablero.llenarTablero();
		}

		/**
		 * Inits the ventana de la zona de juego.
		 * Inicializa la ventana inicial del juego, todas las cartas empiezan ocultas 
		 */
		private void initVentanaZonaDeJuego() {
			// JFrame containerZonaJuego and layout
			containerZonaJuego = getContentPane();
			containerZonaJuego.setLayout(new GridBagLayout());
			panelZonaJuego = new JPanel();
			panelZonaJuego.setLayout(new GridLayout(altoTablero, anchoTablero));
			carta = new JLabel[anchoTablero][altoTablero];
			escuchaCarta = new EscuchaZonaJuego();
			for (int columna = 0; columna < anchoTablero; columna++) {
				for (int fila = 0; fila < altoTablero; fila++) {
					carta[columna][fila] = new JLabel();
					borde = new LineBorder(new Color(24, 23, 0), 3);
					carta[columna][fila].setBorder(borde);
					if (temaSeleccionado == "Naruto")
						carta[columna][fila].setIcon(new ImageIcon("src/imagenes/caraOcultaTema1.jpg"));
					if (temaSeleccionado == "Hora de aventura")
						carta[columna][fila].setIcon(new ImageIcon("src/imagenes/caraOcultaTema2.jpg"));
					carta[columna][fila].addMouseListener(escuchaCarta);
					panelZonaJuego.add(carta[columna][fila]);
				}
			}
			containerZonaJuego.add(panelZonaJuego);
		}
	}

	/**
	 * The Class EscuchaZonaJuego.
	 * Esucha cada carta seleccionada y mira el estado de cada casilla
	 * para continuar o finalizar el juego 
	 */
	private class EscuchaZonaJuego extends MouseAdapter {
		/** The columna anterior en la que se hizo clic. */
		private int columnaAnterior;
		/** The fila anterior en la que se hizo clic. */
		private int filaAnterior;
		/** The columna actual en la que se hizo clic. */
		private int columnaActual;
		/** The fila anterior en la que se hizo clic. */
		private int filaActual;
		/** The numero clicks en el tablero. */
		private int numeroClicksEnTablero;

		/**
		 * Mouse clicked.
		 *
		 * @param event the event
		 */
		public void mouseClicked(MouseEvent event) {                      
			for (int columna = 0; columna < anchoTablero; columna++) {
				for (int fila = 0; fila < altoTablero; fila++) {
					if (event.getSource() == carta[columna][fila]) {
						jugar(columna, fila);
						if (control.terminarJuego()) {
							System.out.println("Juego Terminado"); 
							seguirJugando();
						}
					}
				}
			} 
		}

		/**
		 * Jugar.
		 * compara las cartas seleccionadas, las gira en el caso de que no sean iguales y
		 * también mira el estado del juego. 
		 * 
		 * @param columna the columna
		 * @param fila the fila
		 */
		private void jugar(int columna, int fila) {
			switch (numeroClicksEnTablero) {
			case 0:
				if (!control.estadoCasilla(columna, fila)) {
					control.negarEstadoCasilla(columna, fila);      
					girarCarta(columna, fila, 1);//0 es cara oculta y 1 es cara visible
					numeroClicksEnTablero = 1;
					columnaAnterior = columna;
					filaAnterior = fila;
				}
				break;
			case 1:
				if (!control.estadoCasilla(columna, fila)) {
					control.negarEstadoCasilla(columna, fila);
					girarCarta(columna, fila, 1);
					columnaActual = columna;
					filaActual = fila;
					if (tablero.obtenerCasilla(columnaAnterior, filaAnterior) != tablero.obtenerCasilla(columnaActual, filaActual)){
						control.negarEstadoCasilla(columnaAnterior, filaAnterior);
						control.negarEstadoCasilla(columnaActual, filaActual);
						girarCarta(columnaAnterior, filaAnterior, 0);
						girarCarta(columnaActual, filaActual,0);
						numeroClicksEnTablero = 0;
					}
					else{
						girarCarta(columnaAnterior, filaAnterior, 1);
						girarCarta(columnaActual, filaActual, 1);
						numeroClicksEnTablero = 0;
					}
				}
				break;
			default:
				numeroClicksEnTablero = 0;
				break;
			}
		}

		/**
		 * Girar carta.
		 *
		 * @param columna the columna
		 * @param fila the fila
		 * @param tipo the tipo
		 */
		private void girarCarta(int columna, int fila,  int tipo){   
			switch (tipo) {
			case 0:
				Thread caraOculta = new Thread() {
					public void run() {
						try {
							sleep(500);
							if (temaSeleccionado == "Naruto")
								carta[columna][fila].setIcon(new ImageIcon("src/imagenes/caraOcultaTema1.jpg"));
							if (temaSeleccionado == "Hora de aventura")
								carta[columna][fila].setIcon(new ImageIcon("src/imagenes/caraOcultaTema2.jpg"));
						} catch (InterruptedException ex) {
							ex.printStackTrace();
						}
					}
				};
				caraOculta.start();
				break;
			case 1:
				if (temaSeleccionado == "Naruto")
					carta[columna][fila].setIcon(new ImageIcon("src/imagenes/tema1/"+ tablero.obtenerCasilla(columna, fila) + ".jpg"));
				if (temaSeleccionado == "Hora de aventura")
					carta[columna][fila].setIcon(new ImageIcon("src/imagenes/tema2/" + tablero.obtenerCasilla(columna, fila) + ".jpg"));
				break;
			default:
				break;
			}
		}

		/**
		 * Seguir jugando.
		 */
		private void seguirJugando() {
			String[] Opciones = { "Si quiero c:", "No quiero :c" };
			int eleccion = JOptionPane.showOptionDialog(null,  "¿Quieres volver a jugar?",
					"Ganaste!!!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, 
					new ImageIcon("src/imagenes/caraOculta.jpg"),
					Opciones, Opciones[0]);
			if (eleccion == 0) {
				panelZonaJuego.setVisible(false);
				panelConfiguracionJuego.setVisible(true);
				pack();
				setLocationRelativeTo(null);
			} else {
				System.exit(1);
			}
		}
	}
}