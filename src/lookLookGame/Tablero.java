/*
 * Archivo: Tablero.java
 * Fecha: 24 de Noviembre de 2019
 * Autores: Jaimen Aza              1923556
 *          Valentina Salamanca     1842427
 */
package lookLookGame;

// TODO: Auto-generated Javadoc
/**
 * The Class Tablero. Crea el tablero con valores random del juego, los valores
 * van desde 1 hasta anchoTablero*altoTablero/2.
 * Es necesaria para la clase GUILookLook
 */
public class Tablero{
	
	/** The ancho tablero. */
	private int anchoTablero;
	
	/** The alto tablero. */
	private int altoTablero;
	
	/** The nombre imagen. Almacena los nombres random generados */
	private int nombreImagen[];
	
	/** The tablero. Matriz que guarda el nombre de cada pareja de cartas */
	private int tablero[][];

	/**
	 * Instantiates a new tablero.
	 * Inicializa valores de ancho y alto del tablero y crea una
	 * instancia para el tablero del tamaño de estos valores.
	 * 
	 * @param ancho the ancho
	 * @param alto the alto
	 */
	public Tablero(int ancho, int alto){
		anchoTablero = ancho;
		altoTablero = alto;
		tablero = new int[anchoTablero][altoTablero];
	}

	/**
	 * Create random name vector for image.
	 * Genera un vector con una pareja random de números que corresponde al nombre de cada imagen
	 * en el tablero. 
	 */
	public void createRandomNameImage() {
		nombreImagen = new int[anchoTablero*altoTablero];
		int contador1 = 0;
		while (contador1 < anchoTablero*altoTablero) {
			int randomNameImagen = (int) (Math.random() * (anchoTablero*altoTablero) + 1);
			int contador2 = 0;
			for (int index = 0; index < anchoTablero*altoTablero; index++) {
				if (nombreImagen[index] != randomNameImagen)
					contador2++;
			}
			if (contador2 == anchoTablero*altoTablero) {
				nombreImagen[contador1] = randomNameImagen;
				contador1++;
			}
		}
		int cualImagen = 0;
		for (int i = 0; i < anchoTablero * altoTablero; i++) {
			if (nombreImagen[cualImagen] > (anchoTablero * altoTablero) / 2)
				nombreImagen[cualImagen] = nombreImagen[cualImagen] - (anchoTablero * altoTablero) / 2;
			cualImagen++;
		}
	}

	/**
	 * Llenar tablero.
	 * Genera una matriz que almacena el nombre de cada pareja de imágenes
	 */
	public void llenarTablero() {
		int contador = 1;
		for (int columna = 0; columna < anchoTablero; columna++) {
			for (int fila = 0; fila < altoTablero; fila++) {
				tablero[columna][fila] = nombreImagen[contador-1];
				contador++;
				if (contador > anchoTablero * altoTablero)
					contador = 1;
			}
		}
	}

	/**
	 * Obtener casilla.
	 * Retorna el nombre de la casilla que corresponda a la columna
	 * y fila ingresada. 
	 * 
	 * @param columna the columna
	 * @param fila the fila
	 * @return the int
	 */
	public int obtenerCasilla(int columna, int fila) {
		return tablero[columna][fila];
	}
}