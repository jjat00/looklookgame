/*
 * Archivo: principalLookLookGame.java
 * Fecha: 24 de Noviembre de 2019
 * Autores: Jaimen Aza                1923556
 *          Valentina Salamanca       1842427
 */
package lookLookGame;

import java.awt.EventQueue;

import javax.swing.UIManager;


// TODO: Auto-generated Javadoc
/**
 * The Class principalLookLookGame.
 * Inicia el juego y crea un obejto vista de GUILookLook.
 */
public class principalLookLookGame {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		try {
			String className = UIManager.getCrossPlatformLookAndFeelClassName();
			UIManager.setLookAndFeel(className);
		} catch (Exception e) {}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				GUILooklook vista = new GUILooklook();
			}
		});
	}
}
