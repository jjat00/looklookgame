#**LOOk LOOK GAME**

Se presetan el clásico juego de memoría que en este caso hemos denominado Look look Game

##**INSTRUCCIONES DEL JUEGO**

1. Elegir el tema del juego, existen dos posibilidad:
	*  Naruto 
	*  Hora de aventura

2. Elegir el nivel del juego, hay dos posibilidades:
	*  Básico (Genera un tablero de tamaño 4x3)
	*  Avanzado (Genera un tablero de tamaño 5x4)

3. Hacer clic sobre dos cartas, si estan son iguales las cartas permaneces del lado de la cara visible
   de lo contrario se visualizan 0.5 segundos y vuelven a ocultarse.

4. Una vez encontradas todas las parejas de cartas, hay la posibilidad de seguir jugando o abandonar
   la partida. En caso de decidir seguir jugando se repite el punto 1. 

